#include <stdio.h>
struct data {
    char name[11];
    int math,eng;
};
int main(void) {
    struct data a[5] = {{"小林",84,62},{"鈴木",70,94},{"高橋",65,52},{"田中",96,82},{"山田",82,77}};
    int i;
    printf("名前 Math Eng\n");
    for(i=0;i<5;i++) {
        printf("%s  %d  %d\n",a[i].name,a[i].math,a[i].eng);
    }
    return 0;
}
#include <stdio.h>
struct test_result {
    char name[10];
    int math,eng,total;
    float average;
};
int main(void) {
    struct test_result data[5] = {{"小林",84,62},{"鈴木",70,94},{"高橋",65,52},{"田中",96,82},{"山田",82,77}};
    int i;
    for(i=0;i<5;i++) {
        data[i].total = data[i].math + data[i].eng;
        data[i].average = data[i].total/2.0;
    }
    printf("名前　数学　英語　合計　平均\n");
    for(i=0;i<5;i++) {
        printf("%s %5d %5d %5d %6.1f\n",data[i].name,data[i].math,data[i].eng,data[i].total,data[i].average);
    }
    return 0;
}
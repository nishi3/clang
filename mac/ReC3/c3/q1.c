#include <stdio.h>
#include <string.h>
struct kait_student {
    char gakuseki[10];
    char name[40];
    int age;
};
int main(void) {
    struct kait_student student={"1121999","神奈川　カイト",20};
    puts("構造体メンバを表示します．");
    printf("%s %s %d\n",student.gakuseki,student.name,student.age);
    return 0;
}
#include <stdio.h>
#include <string.h>
struct kait_student {
    char gakuseki[10],name[40];
    int age;
};
int main(void) {
    struct kait_student student;
    printf("学籍番号: ");
    gets(student.gakuseki);
    printf("氏名: ");
    gets(student.name);
    printf("年齢: ");
    scanf("%d", &student.age);
    puts("構造体メンバを表示します．");
    printf("%s %s %d才\n", student.gakuseki, student.name, student.age);
    return 0;
}
#include <stdio.h>
#include <math.h>
typedef struct point {
    double cx,cy;
} Point;
double calc_distance(Point a, Point b) {
    double dis;
    dis = (a.cx - b.cx)*(a.cx - b.cx) + (a.cy - b.cy)*(a.cy - b.cy);
    return sqrt(dis);
}
int main(void) {
    Point a,b;
    puts("A座標");
        printf("x? "); 
        scanf("%lf", &a.cx);
        printf("y? ");
        scanf("%lf", &a.cy);
    puts("B座標");
        printf("x? "); 
        scanf("%lf", &b.cx);
        printf("y? ");
        scanf("%lf", &b.cy);
    printf("2点A，Bの距離は%.1f である。\n", calc_distance(a, b));
    return 0;
}
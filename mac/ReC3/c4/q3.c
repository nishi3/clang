#include <stdio.h>
typedef struct point {
    double cx,cy;
}Point;
typedef struct circle {
    Point pt;
    double r;
}Circle;
Point set_point(double x,double y) {
    Point pt;
    pt.cx = x;
    pt.cy = y;
    return pt;
}
Circle set_circle(Point pt,double r) {
    Circle c;
    c.pt = pt;
    c.r = r;
    return c;
}
int main(void) {
    Point pt;
    Circle c;
    pt = set_point(10.0, 20.0);
    c = set_circle(pt, 5.0);
    printf("円の中心:(%4.1f, %4.1f)\n",c.pt.cx, c.pt.cy);
    printf("半径:r = %4.1f\n",c.r);
    return 0;
}
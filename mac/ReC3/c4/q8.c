#include <stdio.h>
void bubble_sort(int a[], int n);
void print_array(int a[], int n);

int main(void) {
    int a[10] = {20, 6, 55, 74, 3, 45, 13, 87, 46, 30};
    bubble_sort(a,10);
    return 0;
}
void bubble_sort(int a[], int n) {
    int i,j,tmp;
    for(i=0;i<n-1;i++) {
        print_array(a,n);
        for(j=n-1;j>i;j--) {
            if(a[j-1] > a[j]) {
                tmp = a[j];
                a[j] = a[j-1];
                a[j-1] = tmp;
            }
        }
        print_array(a,n);
    }
}
void print_array(int a[], int n) {
    int i;
    static int count = 0;
    printf("%d: ", count++);
    for(i=0;i<n-1;i++) {
        printf("%2d ",a[i]);
    }
    printf("%2d \n",a[i]);
}

// printf("%d: ", count);
// for(i = 0; i < n-1; i++){
//     printf("%2d,", a[i]);
// }
// printf("%2d¥n", a[i]);

#include <stdio.h>
typedef struct point {
    double cx,cy;
}Point;
typedef struct circle {
    Point pt;
    double r;
}Circle;
int is_inside(Circle c, Point pt) {
    double Ans = 0;
    Ans += (c.pt.cx - pt.cx)*(c.pt.cx - pt.cx);
    Ans += (c.pt.cy - pt.cy)*(c.pt.cy - pt.cy);
    if(Ans<=c.r*c.r) return 1;
    else return 0;
}
int main(void) {
    Circle c;
    Point pt;
    printf("X:");
    scanf("%lf",&c.pt.cx);
    printf("Y:");
    scanf("%lf",&c.pt.cy);
    printf("r:");
    scanf("%lf",&c.r);
    printf("X:");
    scanf("%lf",&pt.cx);
    printf("Y:");
    scanf("%lf",&pt.cy);
    if(is_inside(c,pt)) printf("A on Circle\n");
    else printf("A out Circle\n");
    return 0;
}
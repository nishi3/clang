#include <stdio.h>
void address(int *x, double *y);
int main(void) {
    int a = 100;
    double b = 10.28;
    printf("Main\n");
    printf("a=%d   <%p>\n", a, &a);
	printf("b=%2.2f <%p>\n", b, &b);
	address(&a, &b);
    return 0;
}
void address(int *x, double *y) {
    printf("Address\n");
    printf("a=%d <%p>\n",*x,x);
    printf("b=%2.2f <%p>\n",*y,y);
} 
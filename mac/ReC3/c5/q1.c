#include <stdio.h>
int main(void) {
    int a = 100;
    double b = 10.28;
    printf("a = %d\t <%p>\n",a,&a);
    printf("b = %5.2f\t <%p>\n",b,&b);
    return 0;
}
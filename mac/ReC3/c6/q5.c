#include <stdio.h>
int main(void) {
    int buf[5],a,i=0;
    printf("正の整数を入力：\n");
    while(1) {
        scanf("%d",&a);
        if(a == 0) break;
        if(a > 0) {
            buf[i] = a;
            i = (i + 1) % 5;
        }
    }
    printf("buf:\nbuf = ");
    for(i=0;i<5;i++) {
        printf("%d ",buf[i]);
    }
    printf("\n");
    return 0;
}
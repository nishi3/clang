#include <stdio.h>
#include <string.h>
int main(void) {
    int i[3],a;
    double d[3];
    char c[3];
    printf("sizeof(char) =%u byte\n", (unsigned)sizeof(char));
    for(a=0;a<3;a++) {
        printf("a[%d]: %p\n",a,&c[a]);
    }
    printf("sizeof(int)=%lu byte\n", sizeof(int));
	for (a = 0; a < 3; a++) {
		printf("a[%d]=%p\n", a, &i[a]);
	}
    printf("sizeof(double)=%lu byte\n", sizeof(double));
	for (a = 0; a < 3; a++) {
		printf("a[%d]=%p\n", a, &d[a]);
	}
	return 0;
}
#include <stdio.h>
#include <string.h>
int mystrlen(char[]);
int main(void) {
    char str[] = "I love JavaScript";
    int len;
    printf("%s\n",str);
    len = mystrlen(str);
    printf("mystrlenの実行結果;\n");
    printf("-> %d 文字\n",len);
    return 0;
}
int mystrlen(char str[]) {
    int count = 0;
    while(str[count] != '\0') {
        count++;
    }
    return count;
}
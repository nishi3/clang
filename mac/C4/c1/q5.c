#include  <stdio.h>

/* 平均を求める */
double average(int a[], int n) {
    long long sum = 0;
    int i;

    for (i = 0; i < n; i++){
        sum += a[i];
    }
    return (double) sum / n;
}

/* 配列内のデータを表示 */
void show(int x[], int n) {
    int i;
    for (i = 0; i < n; i++) {
        printf("%d ", x[i]);
    }
}

int main(void) {
    int x1[] = { 1, 2, 3, 4, 5 };
    int len_x1 = sizeof(x1) / sizeof(x1[0]);

    int x2[] = { 1000000000, -2000000000, -1000000000, 2000000000 };
    int len_x2 = sizeof(x2) / sizeof(x2[0]);

    int x3[] = { 1000000000, 2000000000, 1000000000, 2000000000 };
    int len_x3 = sizeof(x3) / sizeof(x3[0]);

    show(x1, len_x1);
    printf("\n");
    printf("の平均は%fです．\n", average(x1, len_x1));
    printf("\n");

    show(x2, len_x2);
    printf("\n");
    printf("の平均は%fです．\n", average(x2, len_x2));
    printf("\n");

    show(x3, len_x3);
    printf("\n");
    printf("の平均は%fです．\n", average(x3, len_x3));
    printf("\n");

}
#include  <stdio.h>
#include <string.h>

/* 選択ソート */
void selection_sort(char* a[], int n) {
	int i, j, min_idx;
	char* min;
	for (i = 0; i < n; i++){
		// a[i+1]からa[n-1]までの最小値を求めてminに代入，minが格納されているインデックスをmin_idxに代入
		min = a[i];
		min_idx = i;
		for (j = i + 1; j < n; j++){
			if (a[j] < min) {
				min = a[j];
				min_idx = j;
			}
		}
		// a[i]とa[min_idx]を入れ替える（a[min_idx]の値はminと等しい）
		a[min_idx] = a[i];
		a[i] = min;
	}
}

/* 配列内のデータを表示 */
void show(char* a[], int n) {
	int i;
	for (i = 0; i < n; i++) {
		printf("\"%s\" ", a[i]);
	}
	printf("\n");
}

void main(void) {
	char* x1[] = { "Fukuoka", "Kanagawa", "Kagoshima", "Kyoto", "Nagano", "Shizuoka", "Tokyo", "Osaka" };
	int x1_len = sizeof(x1) / sizeof(x1[0]);
	printf("x1 ソート前: ");
	show(x1, x1_len);
	selection_sort(x1, x1_len);
	printf("x1 ソート後: ");
	show(x1, x1_len);
	printf("\n");

	char* x2[] = { "Tokyo", "Kanagawa", "Shizuoka", "Osaka" };
	int x2_len = sizeof(x2) / sizeof(x2[0]);
	printf("x2 ソート前: ");
	show(x2, x2_len);
	selection_sort(x2, x2_len);
	printf("x2 ソート後: ");
	show(x2, x2_len);
	printf("\n");

	char* x3[] = { "Shizuoka", "Osaka", "Kanagawa", "Nagano", "Kyoto" };
	int x3_len = sizeof(x3) / sizeof(x3[0]);
	printf("x3 ソート前: ");
	show(x3, x3_len);
	selection_sort(x3, x3_len);
	printf("x3 ソート後: ");
	show(x3, x3_len);
	printf("\n");
}
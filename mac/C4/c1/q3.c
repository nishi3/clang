#include <stdio.h>
#include <math.h>

int main(void) {
    static const double epsilon = 0.01;
    double x = 0.0;
    while (1) {
        if (fabs(x - 1.0) < epsilon) break;
        x = x + 0.1;
        printf("%1.1f\n", x);
    }
}
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

enum janken {Goo, Choki, Pa};

int main(void) {
    // int computer_no_te;
    enum janken computer_no_te;
    srand(time(NULL));
    computer_no_te = rand()%3;
    printf("computer_no_te = %d\n", computer_no_te);
    switch(computer_no_te) {
        case Goo: printf("コンピュータの手は「グー」です.\n"); break;
        case Choki: printf("コンピュータの手は「チョキ」です.\n"); break;
        case Pa: printf("コンピュータの手は「パー」です.\n"); break;
    }
    return 0;
}
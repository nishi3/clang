#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 10

int main(void){
    int tmp[N];
    int i,j,k,idx;
    srand(time(NULL));
    for(i=0; i<N; i++) tmp[i] = i;
    printf("並び替えの結果を表示します\n"); printf("=> ");
    for(i=N; i>0; i--) {
        idx = rand()%i; printf("%d ", tmp[idx]);
        for(j=idx; j<i-1; j++) {
        tmp[j] = tmp[j+1];
        }
    }
    printf("\n");
    return 0;
}
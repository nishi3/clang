#include <stdio.h>

int factorial (int);
int main(void) {
    int i;
    for(i=1; i<=8; i++) {
        printf("%d! = %d \n", i, factorial(i));
    }
    return 0;
}

int factorial (int x) {
    if(x<=0) return 1;
    return x*factorial(x-1);
}
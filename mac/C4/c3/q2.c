#include <stdio.h>

enum operation {Add, Sub, Mul, Div};

int main(void) {
    enum operation op;
    int x,y;
    while(1){
        printf("加(0)，減(1)，乗(2)，除(3): "); 
        scanf("%d", &op); 
        if(op<Add || op>Div) break;
        //first
        printf("1番目の数:"); 
        scanf("%d", &x); 
        //second
        printf("2番目の数:"); 
        scanf("%d", &y); 
        switch(op){
            case Add: printf("加算の結果は%dです。\n", x+y); break;
            case Sub: printf("減算の結果は%dです。\n", x-y); break;
            case Mul: printf("乗算の結果は%dです。\n", x*y); break;
            case Div: 
                if(y!=0) printf("除算の結果は%dです。\n",x/y);
                else printf("ゼロ割りが発生しました.\n");
        } 
    }
    puts("プログラムを終了します.");
    return 0;
}
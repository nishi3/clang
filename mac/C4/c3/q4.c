#include <stdio.h>

int sum(int);
int main(void) {
    int n,s;
    printf("n? ");
    scanf("%d",&n);
    s = sum(n);
    if(s<0) printf("計算できません。\n");
    else printf("1からnまでの総和は%dです。\n", sum(n));
    return 0;
}

int sum(int n) {
    if(n<1) return -1;
    else if(n==1) return 1;
    return sum(n-1) + n;
}

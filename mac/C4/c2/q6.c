#include <stdio.h>
#define N 5
int main(void) {
    char *strtbl[N] = { "apple","banana","lemon","melon","orange" };
    int i;
    
    for(i=0; i<N; i++) {
        printf("%d: %s\n", i, strtbl[i]);
    }

    return 0;
}
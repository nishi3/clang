#include <stdio.h>
int mycopy(char str[], char *p);
int main(void) {
    char a[80] = "Empty",*b = "Information";
    
    printf("配列 a の内容(前):%s\n", a);
    mycopy(a, b);
    printf("配列 a の内容(後):%s\n", a);

    return 0;
}

int mycopy(char str[], char *p) {
    int i = 0;
    while(*p){
        str[i++]=*p++;
    }
    str[i] = '\0';
    return 0;
}
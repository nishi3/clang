#include <stdio.h>
int mylength(char *p);
int main(void) {
    char *a = "Information";
    
    printf("配列 a の長さ: %d 文字\n", mylength(a));
    
    return 0;
}

int mylength(char *p) {
    int count = 0;
    while(*p++){
        count++;
    }
    return count;
}

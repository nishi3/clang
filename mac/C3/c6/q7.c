#include <stdio.h>
#include <string.h>
struct student {
	char name[20];
	int height;
	int weight;
};
int main(void) {
	struct student x, *px;
	px = &x;
	strcpy((*px).name, "鈴木太郎");
	px->height = 170;
	px->weight = 63;
	printf("name=%s\n", px->name);
	printf("weight=%d\n", px->weight);
	printf("height=%d\n", px->height);
	return 0;
}
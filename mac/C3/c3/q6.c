#include <stdio.h>
#include <math.h>
struct point{
    double cx, cy;
};

int main(void) {
    struct point a, b;
    double d, sum = 0;
    printf("点Aの座標\n");
    printf("x? "); scanf("%lf", &a.cx);
    printf("y? "); scanf("%lf", &a.cy);
    printf("点Bの座標\n");
    printf("x? "); scanf("%lf", &b.cx);
    printf("y? "); scanf("%lf", &b.cy);
    printf("点Aの座標 (%.1f,%.1f)\n", a.cx, a.cy);
    printf("点Bの座標 (%.1f,%.1f)\n", b.cx, b.cy);
    sum += (a.cx - b.cx)*(a.cx - b.cx);
    sum += (a.cy - b.cy)*(a.cy - b.cy);
    d = sqrt(sum);
    printf("2点A，Bの距離は%.1f である.\n", d);
    return 0;
}
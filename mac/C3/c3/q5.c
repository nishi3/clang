#include <stdio.h>
struct point{
    double cx, cy;
};

int main(void) {
    struct point a, b;
    printf("点Aの座標\n");
    printf("x? "); scanf("%lf", &a.cx);
    printf("y? "); scanf("%lf", &a.cy);
    printf("点Bの座標\n");
    printf("x? "); scanf("%lf", &b.cx);
    printf("y? "); scanf("%lf", &b.cy);
    printf("点Aの座標 (%0.1f,%0.1f)\n", a.cx, a.cy);
    printf("点Bの座標 (%0.1f,%0.1f)\n", b.cx, b.cy);
    return 0;
}
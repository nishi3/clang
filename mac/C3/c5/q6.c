#include <stdio.h>

void swap_v(int nx, int ny) {
   int tmp = nx;
   nx = ny;
   ny = tmp;
}
void swap_r(int *nx, int *ny) {
   int tmp = *nx;
   *nx = *ny;
   *ny = tmp;
}
int main(void) {
   int nx = 10, ny = 20;
printf("nx = %d, ny = %d\n", nx, ny);
swap_v(nx, ny);
puts("swap_v の実行後:");
printf("nx = %d, ny = %d\n", nx, ny);
swap_r(&nx, &ny);
puts("swap_r の実行後:");
printf("nx = %d, ny = %d\n", nx, ny);
return 0; 
}
#include <stdio.h>

int filter(int*);
int main(void) {
	int a;
	putchar('>');
	scanf("%d", &a);
	a = filter(&a);
	printf("a：%d\n", a);
	return 0;
}
int filter(int *x) {
	if (*x<0 || *x>100) *x = -1;
	else if (*x<50) *x = 0;
	else if (*x<80) *x = 1;
	else *x = 2;
	return *x;
}
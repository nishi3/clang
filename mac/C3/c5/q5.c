#include<stdio.h>

void adress(int *x,double *y);
int main(void) {
	int a = 100;
	double b = 10.28;
	printf("===main側の表示 ===\n");
	printf("a=%d   <%p>\n", a, &a);
	printf("b=%2.2f <%p>\n", b, &b);
	adress(&a, &b);
	return 0;

}

void adress(int *x, double *y) {
	printf("=== adress側の表示 ===\n");
	printf("a=%d   <%p>\n", *x, x);
	printf("b=%2.2f <%p>\n", *y, y);
}
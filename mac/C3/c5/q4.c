#include<stdio.h>

int main(void) {
	int x=10, y=20, z=30, *pr1, *pr2;
	pr1 = &x;
	pr2 = &y;
	printf("=== 処理前 ===\n");
	printf("x = %d y = %d z = %d\n", x, y, z);
	*pr1 = *pr1 + *pr2;
	pr2 = &z;
	*pr2 = *pr1 + *pr2;
	pr1 = &y;
	*pr1 = *pr1 + *pr2;
	printf("=== 処理後 ===\n");
	printf("x = %d y = %d z = %d\n", x, y, z);
	return 0;
}
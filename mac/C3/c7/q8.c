#include<stdio.h>
#include<stdlib.h>
#define N 3
int main(void) {
    char name[20];
    FILE *fp, *fx;

    if((fp = fopen("A.txt", "a")) == NULL) {
        printf("File Open Error!\n");
        exit(1);
    }

    if((fx = fopen("B.txt", "r")) == NULL) {
        printf("File Open Error!\n");
        exit(1); 
    }

    while (fgets(name, N, fx)) {
		fprintf(fp, "%s", name);
	}
	fclose(fp);
	fclose(fx);
	return 0;
}   
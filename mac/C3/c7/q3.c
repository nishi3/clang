#include<stdio.h>
#include<stdlib.h>
int main(void) {
	char name[21], tel[16];
	int age;
	FILE *fp;
	if ((fp = fopen("mydata.dat", "r")) == NULL) {
		printf("file open error\n");
		exit(1);
	}
	fscanf(fp, "%s%d%s", name, &age, tel);
	printf("名前: %s\n", name);
    printf("年齢: %d\n", age);
    printf("電話番号: %s\n", tel);
	fclose(fp);
	return 0;
}
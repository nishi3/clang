#include <stdio.h>
#include <stdlib.h>
int main(void) {
    int age;
    char name[21], tel[16];
    FILE *fp;

    printf("名前は？ ");
    scanf("%s", name);
    printf("年齢は？ ");
    scanf("%d", &age);
    printf("電話番号は？ ");
    scanf("%s", tel);

    if((fp = fopen("mydata.dat", "w")) == NULL) {
        printf("file open error\n");
        exit(1);
    }
    fprintf(fp, "%5s %2d %2s \n", name, age, tel);
    printf("ファイルへの書き込みを完了しました.\n");
    fclose(fp);
    return 0;
}
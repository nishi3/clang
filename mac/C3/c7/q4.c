#include <stdio.h>
#include <stdlib.h>
int main(void) {
    char name[21] = "情報花子";
    int age = 20;
    char tel[16] = "045-73-xxxx";
    FILE *fp;

    if((fp = fopen("mydata.dat", "a")) == NULL) {
        printf("file open error\n");
        exit(1);
    }
    fprintf(fp, "%s %d %s \n",name, age, tel);
    fclose(fp);
    return 0;
}
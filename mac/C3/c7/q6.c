#include <stdio.h>
#include <stdlib.h>
int main(void) {
    int ch;
    FILE *fp;

    if((fp = fopen("q2.c", "r")) == NULL) {
        printf("file open error\n");
        exit(1);
    }
    while((ch = fgetc(fp)) != EOF) {
        putchar(ch);
    }
    fclose(fp);
    return 0;
}
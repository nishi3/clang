#include <stdio.h>
#include <string.h>

int main(void) {
    char str[81],strar[5][10] = { "apple", "banana", "lemon", "melon", "orange" };
    int len,idx;
    while(1) {
        putchar('>');
        gets(str);
        len = strlen(str);
        if(len == 0){
            puts("プログラムを終了します."); break; 
        }
        for(idx=0; idx<5; idx++){
            if(strcmp(str, strar[idx])==0) break;
        }
        if(idx==5) puts("登録されていません");
        else printf("idx:%d 番に登録されています\n", idx);
    }
    return 0;
}
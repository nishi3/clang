#include <stdio.h>
#include <string.h>

int main(void) {
    int ans;
    char str1[81], str2[81];
    printf("文字列1: ");
    gets(str1);
    printf("文字列2: ");
    gets(str2);
    ans = strcmp(str1, str2);
    if(ans == 0)
       printf("%s = %s\n", str1, str2);
    else if(ans < 0)
       printf("%s < %s\n", str1, str2);
    else
       printf("%s < %s\n", str2, str1);
       
    return 0;
}
#include <stdio.h>
struct test_result {
   char name[10];
   int math;
   int eng;
};

int main(void) {
    struct test_result data[5] = { {"小林", 84, 62}, {"鈴木", 70, 94}, {"高橋", 65, 52}, {"田中", 96, 82},{"山田", 82, 77} };
    struct test_result tmp;
    int i,j;
    for(i=0; i<5; i++) {
        for(j=i+1; j<5; j++) {
            if(data[i].math < data[j].math) {
                tmp = data[i];
                data[i] = data[j];
                data[j] = tmp;
            }
        }
    }
    puts("数学の成績:"); 
    for(i=0; i<5; i++) {
        printf("%d: %s %4d\n", i+1, data[i].name, data[i].math);
    }
    for(i=0; i<5; i++) {
        for(j=i+1; j<5; j++){
            if(data[i].eng < data[j].eng) {
                tmp = data[i];
                data[i] = data[j];
                data[j] = tmp;
            }
        }
    }
    puts("英語の成績:");
    for(i=0; i<5; i++) {
        printf("%d: %s %4d\n", i+1, data[i].name, data[i].eng);
    }
    return 0;
}
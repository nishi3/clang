#include <stdio.h>
typedef struct point {
    double cx;
    double cy;
} Point;
typedef struct circle {
    Point pt;
    double r;
} Circle;
int is_inside(Circle c, Point pt) {
    double Ans = 0;
    Ans += (c.pt.cx - pt.cx)*(c.pt.cx - pt.cx);
    Ans += (c.pt.cy - pt.cy)*(c.pt.cy - pt.cy);
    if(Ans<=c.r*c.r) return 1;
    else return 0;
}

int main(void) {
    Circle c;
    Point pt;
    printf("中心点の x 座標: ");
    scanf("%lf", &c.pt.cx);
    printf("中心点の y 座標: ");
    scanf("%lf", &c.pt.cy);
    printf("半径: ");
    scanf("%lf", &c.r);
    printf("点の x 座標: ");
    scanf("%lf", &pt.cx);
    printf("点の y 座標: ");
    scanf("%lf", &pt.cy);
    if(is_inside(c, pt)) printf("点Aは円の内部に含まれる。\n");
    else printf("点Aは円の内部に含まれない。\n");
    return 0;
}
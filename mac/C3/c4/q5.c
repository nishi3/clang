#include <stdio.h>
typedef struct point {
    double cx;
    double cy;
} Point;
typedef struct circle {
    Point pt;
    double r;
} Circle;

int main(void) {
    Circle c;
    printf("中心点の x 座標: ");
    scanf("%lf", &c.pt.cx);
    printf("中心点の y 座標: ");
    scanf("%lf", &c.pt.cy);
    //なぜこれを入れさせたのか？？
    printf("半径: ");
    scanf("%lf", &c.r);
    printf("円Cの面積 → %.2f\n", 3.14159*c.r*c.r);
    return 0;
}
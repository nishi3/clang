#include <stdio.h>
void init_array(int[]);
int main(void) {
    int i,z,abc[5];
    init_array(abc);
    for(i=0;i<5;i++) {
        printf("abc[%d]=%d\n",i,abc[i]);
    }
    return 0;
}
void init_array() {
    int i,z[5];
    for(i=0;i<5;i++) {
        z[i] = -1;
    }
    return z;
}
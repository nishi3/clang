#include<stdio.h>

int main(void) {
	char a[] = "abcd", *p="abcd";
	printf("%p\n", a[1]);
	printf("%p\n", p);
	printf("%p\n\n", "abcd");

	printf("%s", a);
	a[0] = '@';
	printf("%s\n", a);

	printf("*pが示す内容:%c\n", p);
	p[0] = '@';
	printf("%s", p);
	return 0;
}
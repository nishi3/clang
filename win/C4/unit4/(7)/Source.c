#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#define N 1

int main(void) {
	int tmp[10] = { 0,1,2,3,4,5,6,7,8,9}, idx,i,m=0;

	for (i = 9; i >= N;i--){
		srand(time(NULL));
		idx = rand() % i;
		printf("%d ", tmp[idx]);

		if (idx != i) {
			m = tmp[idx];
			tmp[idx] = tmp[i];
			tmp[i] = m;
		}
	}
	printf("%d\n", tmp[0]);
	return 0;
}
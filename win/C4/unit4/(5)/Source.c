#include<stdio.h>
#define N 8

void margesort(int A[], int p, int r);
void marge(int A[], int p, int q, int r);

int main(void) {
	int a[9] = { 5,2,4,6,2,1,3,2,6 }, i;
	printf("変化まえ");
	for (i = 0; i < 9; i++) printf("%d,", a[i]);
	printf("\n");
	margesort(a, 0, 5);
	printf("変化ご");
	for (i = 0; i < 9; i++) printf("%d,", a[i]);
	printf("\n");
	return 0;
}

void margesort(int A[], int p, int r) {
	int q = 0;
	if (p < r) {
		q = (p + r) / 2;
		margesort(A, p, q);
		margesort(A, q + 1, r);
		marge(A, p, q, r);
	}
}

void marge(int A[], int p, int q, int r) {
	int i, j, k;
	int B[N];
	if (p == r) return;
	for (i = p; i <= r; i++) {
		B[i] = A[i];
	}

	i = p; j = q + 1; k = p;
	while (i <= q&&j <= r) {
		if (B[i] < B[j]) {
			A[k++] = B[i++];
		}
		else {
			A[k++] = B[j++];
		}
	}

	if (i > q) {
		while (k <= r) {
			A[k++] = B[j++];
		}
	}
	else if (j>r) {
		while (k <= r) {
			A[k++] = B[i++];
		}
	}
}
#include  <stdio.h>

/* バブルソート */
void bubble_sort(int a[], int n) {
	int i, j, tmp;
	for (i = 0; i < n - 1; i++) {
		// iからn-1までの隣接する要素を比較する
		for (j = n - 1; j > i; j--) {
			// a[j-1] > a[j] ならa[j-1]とa[j]を入れ替える
			if (a[j - 1] > a[j]) {
				tmp = a[j];
				a[j] = a[j - 1];
				a[j - 1] = tmp;
			}
		}
	}
}

/* 配列内のデータを表示 */
void show(int x[], int n) {
	int i;
	for (i = 0; i < n; i++) {
		printf("%d\t", x[i]);
	}
	printf("\n");
}

void main(void) {
	int x[] = { 9, 4, 6, 2, 1, 8, 0, 3, 7, 5 };    /* ソートするデータ */
	int len = sizeof(x) / sizeof(x[0]);

	printf("ソート前:\n");
	show(x, len);
	bubble_sort(x, len);

	printf("ソート後:\n");
	show(x, len);
}
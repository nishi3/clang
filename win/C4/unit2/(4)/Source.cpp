#include  <stdio.h>

/* 選択ソート */
void selection_sort(long long int a[], long long int n) {
	int min;
	int i, j, min_idx;
	for (i = 0; i < n; i++) {
		// a[i+1]からa[n-1]までの最小値を求めてminに代入，minが格納されているインデックスをmin_idxに代入
		min = a[i]; min_idx = i;
		for (j = i + 1; j < n; j++) {
			if (a[j] < min) {
				min = a[j];
				min_idx = j;
			}
		}
		// a[i]とa[min_idx]を入れ替える（a[min_idx]の値はminと等しい）
		a[min_idx] = a[i];
		a[i] = min;
	}
}

/* 配列内のデータを表示 */
void show(long long int x[], long long int n) {
	int i;
	for (i = 0; i < n; i++) {
		printf("%lld ", x[i]);
	}
	printf("\n");
}

void main(void) {
	long long int x1[] = { -123, 4567, 8901234, 267890987, -2109876543, 56765, 4321234 };    /* ソートするデータ */
	int len_x1 = sizeof(x1) / sizeof(x1[0]);

	long long int x2[] = { 1000, -2000000, 3000000000, -40000, 5000000 };    /* ソートするデータ */
	int len_x2 = sizeof(x2) / sizeof(x2[0]);

	printf("ソート前: ");
	show(x1, len_x1);
	selection_sort(x1, len_x1);
	printf("ソート後: ");
	show(x1, len_x1);
	printf("\n");

	printf("ソート前: ");
	show(x2, len_x2);
	selection_sort(x2, len_x2);
	printf("ソート後: ");
	show(x2, len_x2);
	printf("\n");
}
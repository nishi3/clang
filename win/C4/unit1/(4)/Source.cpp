#include<stdio.h>
struct point
{
	double x, y;
};
typedef struct rectangle {
	struct point p1,p2;
}rectangle;

double area(rectangle *r);
int main(void) {
	rectangle r;
	struct point p;
	double s;

	printf("座標1x->y");
	scanf("%lf%lf", &r.p1.x,&r.p1.y);
	printf("座標2x->y");
	scanf("%lf%lf", &r.p2.x,&r.p2.y);
	s = area(&r);

	printf("面積:%f\n",s);

	return 0;
}

double area(rectangle *r) {
	double disx, disy;
	disx = (r->p1.x > r->p2.x) ? r->p1.x - r->p2.x : r->p2.x - r->p1.x;
	disy = (r->p1.y > r->p2.y) ? r->p1.y - r->p2.y : r->p2.y - r->p1.y;

	return disx*disy;
}
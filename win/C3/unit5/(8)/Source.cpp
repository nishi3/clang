#include <stdio.h>
void filter(int*);
int main(void) {
	int a[10] = { 87, 56, 76, 32, 19, 94, 21, 88, 65, 42 },i;
	for (i = 0; i < 10; i++) {
		printf("%d ", a[i]);
	}
	printf("\n=== filter�� ===\n");
	for (i = 0; i < 10; i++) {
		filter(&a[i]);
		printf("%d ", a[i]);
	}
	printf("\n");
	return 0;
}

void filter(int *x) {
	if (*x<0 || *x>100) *x = -1;
	else if (*x<50) *x = 0;
	else if (*x<80) *x = 1;
	else *x = 2;
}
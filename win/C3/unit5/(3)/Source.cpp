#include<stdio.h>

int main(void) {
	int x=4, y=25, z, *a, *b;
	a = &x;
	b = &y;
	z = *a * *b;
	printf("a=%d b=%d\n", *a, *b);
	printf("z=%d\n", z);
	return 0;
}
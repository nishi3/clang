#include<stdio.h>

typedef struct point {
	double cx, cy;
}Point;

typedef struct circle {
	Point pt;
	double r;
}Circle;

struct point set_point(double x, double y) {
	Point pt;
	pt.cx = x;
	pt.cy = y;
	return pt;
}

struct circle set_circle(Point a, double x) {
	Circle c;
	c.pt = a;
	c.r = x;
	return c;
}

int main(void) {
	Point pt;
	Circle c;
	pt = set_point(10.0,20.0);
	c = set_circle(pt, 5.0);
	printf("�~�̒��S:(%4.1f,%4.1f)\n", c.pt.cx, c.pt.cy);
	printf("���a:r=%4.1f\n", c.r);
	return 0;
}
#include<stdio.h>
#include<math.h>
typedef struct point {
	double cx, cy;
}Point;

typedef struct circle {
	Point pt;
	double r;
}Circle;

int is_inside(Circle c, Point pt);

int main(void) {
	Point pt2;
	Circle c;

	printf("中心点のx座標");
	scanf("%lf", &c.pt.cx);
	printf("中心点のy座標");
	scanf("%lf", &c.pt.cy);
	printf("半径");
	scanf("%lf", &c.r);

	printf("点Aのx座標:");
	scanf("%lf", &pt2.cx);
	printf("点Aのy座標:");
	scanf("%lf", &pt2.cy);
	if (is_inside(c, pt2) == 1) {
		printf("点Aは円の内部に含まれる\n");
	}
	else
	{
		printf("点Aは円の内部に含まれない\n");
	}
	return 0;
}

int is_inside(Circle c, Point pt) {
	double l,sum;
	l = sqrt((c.pt.cx - pt.cx)*(c.pt.cx - pt.cx) + (c.pt.cy - pt.cy)*(c.pt.cy - pt.cy));
	if (l <= c.r) {
		return 1;
	}
	else{
		return 0;
	}
}